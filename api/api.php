<?php
include "connection.php";
header('Content-Type: application/json');

if(isset($_POST['method'])){

    $method = $_POST['method'];
    $response = array();

    switch ($method) {
        case "registerUser":

            $userTable = "users";

            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $email = $_POST['email'];
            $username = $_POST['username'];
            $password = $_POST['password'];
            $phone = $_POST['phone'];
            $role = $_POST['role'];
            $fromDashboard = $_POST['fromDashboard'];

            if (isset($fromDashboard) || $fromDashboard == "true") {
                $role = "w";
            }

            //validation
            if (isset($name) && isset($surname) && isset($email) && isset($username) && isset($password) && isset($role)
                && trim($name) != "" && trim($surname) != "" && trim($email) != "" && trim($username) != "" && trim($password) != "") {

                //validate email
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $response['response'] = "Error";
                    $response['message'] = "Please enter a valid email";
                    echo json_encode($response);
                } //validate username
                else if (strlen($username) < 5) {
                    $response['response'] = "Error";
                    $response['message'] = "Username must have at least 5 characters";
                    echo json_encode($response);
                } //check if role is valid
                else if ($role != "c" && $role != "a" && $role != "w") {
                    $response['response'] = "Error";
                    $response['message'] = "Not a valid user role";
                    echo json_encode($response);
                } //validate password
                else if (strlen($password) < 8) {
                    $response['response'] = "Error";
                    $response['message'] = "Password must be at least 8 digits";
                    echo json_encode($response);
                } //regex to validate password
                else if (!preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $password)) {
                    $response['response'] = "Error";
                    $response['message'] = "Password must contain letters and numbers";
                    echo json_encode($response);
                } //all inputs correct
                else {

                    //check if username is taken
                    $checkUsername = "SELECT user_id FROM $userTable WHERE username='$username'";
                    $result = mysqli_query($conn, $checkUsername);

                    if (mysqli_num_rows($result) > 0) {
                        $response['response'] = "Error";
                        $response['message'] = "Username taken!";
                        echo json_encode($response);
                    } else {

                        //check if user exists
                        $checkEmail = "SELECT user_id FROM $userTable WHERE email='$email'";
                        $result = mysqli_query($conn, $checkEmail);

                        //user doesnt exists
                        if (mysqli_num_rows($result) == 0) {

                            //password encryption
                            $passMD5 = md5($password);
                            $addUserQuery = mysqli_query($conn, "INSERT INTO $userTable(name, surname, email, username, password, phone, role) 
                                 VALUES('$name', '$surname', '$email', '$username', '$passMD5', '$phone', '$role')");

                            if (!$addUserQuery) {
                                $response['response'] = "Error";
                                $response['message'] = "Error adding user: " . mysqli_error($conn);
                                echo json_encode($response);
                            } else {
                                $response['response'] = "Success";
                                $response['message'] = "User Added";
                                echo json_encode($response);

                                if ($role == "c") {
                                    setcookie("LoggedIn", "c", time() + (86400 * 30), "/");
                                    setcookie("LoggedInRole", "c", time() + (86400 * 30), "/");
                                }
                            }
                        } else {
                            $response['response'] = "Error";
                            $response['message'] = "User already exists";
                            echo json_encode($response);
                        }

                    }

                }

            } else {
                $response['response'] = "Error";
                $response['message'] = "Please Fill All Required Fields";
                echo json_encode($response);
            }
            break;
        case "searchWorkers":
            if (!isset($_POST['name'])) {
                $response['response'] = "Error";
                $response['message'] = "No given value";
                echo json_encode($response);
            } else {
                $name = $_POST['name'];
                $sql = "SELECT user_id, name, surname FROM users where role='w' AND name LIKE'$name%' LIMIT 15";
                $result = mysqli_query($conn, $sql);
                if (mysqli_num_rows($result) < 1) {
                    $response['response'] = "Error";
                    $response['message'] = "No values";
                    echo json_encode($response);
                } else {
                    $dataArray = array();
                    while ($row = mysqli_fetch_assoc($result)) {
                        array_push($dataArray, $row);
                    }
                    $response['response'] = "Success";
                    $response['message'] = "Values received";
                    $response['workers'] = $dataArray;
                    echo json_encode($response);

                }
            }

            break;
        case "checkWorkerLogIn":
            if (!isset($_POST['user_id'])) {
                $response['response'] = "Error";
                $response['message'] = "No values";
                echo json_encode($response);
            }
            $userID = $_POST['user_id'];
            $sql = "SELECT * FROM worker_table where user_id=$userID";
            if ($result = mysqli_query($conn, $sql)) {
                if (mysqli_num_rows($result) > 0) {
                    $response['response'] = "Success";
                    $response['message'] = "User is set";
                } else {
                    $response['response'] = "Success";
                    $response['message'] = "User is not set";
                }


                echo json_encode($response);
            } else {
                $response['response'] = "ERROR";
                $response['message'] = "Error connecting server " . mysqli_error($conn);
                echo json_encode($response);
            }

            break;
        case "requestProfessions":
            $sql = "SELECT profession_name FROM profession ORDER BY id_profession ASC";
            $result = mysqli_query($conn, $sql);
            if (mysqli_num_rows($result) < 1) {
                $response['response'] = "Error";
                $response['message'] = "No values";
                echo json_encode($response);
            } else {
                $dataArray = array();
                while ($row = mysqli_fetch_assoc($result)) {
                    array_push($dataArray, $row["profession_name"]);
                }

                $response['response'] = "Success";
                $response['message'] = "Values received";
                //$response['user_id'] = "" . $_SESSION["LoggedInUserId"];
                $response['professions'] = $dataArray;
                echo json_encode($response);

            }

            break;
        case "saveWorkersData":
            if (!isset($_POST['job']) || !isset($_POST['address'])) {
                $response['response'] = "Error";
                $response['message'] = "Please enter all values";
                echo json_encode($response);
            } else {
                $job = $_POST['job'];
                $address = $_POST['address'];
                $description = $_POST['description'];
                $user_id = $_COOKIE['LoggedIn'];
                $sql = "INSERT INTO worker_table(description, job, address, user_id) VALUES( '$description', '$job', '$address', '$user_id')";
                if (mysqli_query($conn, $sql)) {
                    $response['response'] = "Success";
                    $response['message'] = "Worker added";
                    echo json_encode($response);
                } else {
                    $response['response'] = "Error";
                    $response['message'] = "Error adding worker";
                    echo json_encode($response);
                }
            }
            break;
        case "loggedInUser":
            if (isset($_COOKIE['LoggedIn'])) {
                $response['response'] = "Success";
                $response['message'] = $_COOKIE['LoggedIn'];
                echo json_encode($response);
            }else{
                $response['response'] = "Error";
                $response['message'] = "User not logged in";
                echo json_encode($response);
            }
            break;
        case "getUserByID":
            if(isset($_POST['user_id'])){
                $id = $_POST['user_id'];

                $sql = ("SELECT * FROM users WHERE user_id=$id");
                if ($result = mysqli_query($conn, $sql)) {
                    $row = mysqli_fetch_assoc($result);
                    $response['response'] = "Success";
                    $response['user_name'] = $row['name'];
                    echo json_encode($response);
                } else {
                    $response['response'] = "Error";
                    $response['message'] = "Error";
                    echo json_encode($response);
                }
            }else{
                $response['response'] = "Error";
                $response['message'] = "Error";
                echo json_encode($response);
            }
            break;
        case "addWorkerExperience":
            if(isset($_POST['jobTitle']) && isset($_POST['user_id'])){
                if(isset($_POST['dateFrom'])){
                    $dateFrom = $_POST['dateFrom'];
                }
                if(isset($_POST['dateTo'])){
                    $dateTo = $_POST['dateTo'];
                }
                if(isset($_POST['jobDescription'])){
                    $jobDescription = $_POST['jobDescription'];
                }

                $jobTitle = $_POST['jobTitle'];
                $userID = $_POST['user_id'];

                $sql = "INSERT INTO experience (worker_id,  job_name, beginning_Date, ending_Date, description) VALUES(
                        (SELECT w.id FROM users u
                         INNER JOIN worker_table w ON u.user_id = w.user_id
                         WHERE u.user_id = $userID),
                          '$jobTitle', '$dateFrom', '$dateTo', '$jobDescription');";
                if(mysqli_query($conn, $sql)){
                    $response['response'] = "Success";
                    $response['message'] = "Experience Added";
                    echo json_encode($response);
                }else{
                    $response['response'] = "Error";
                    $response['message'] = "Experience not added";
                    echo json_encode($response);
                }
            }else{
                $response['response'] = "Error";
                $response['message'] = "Experience not added";
                echo json_encode($response);
            }
            break;
        case "getWorkerExperiences":
            if(isset($_POST['user_id'])){
                $userID = $_POST['user_id'];
                $sql = "SELECT * FROM experience e
                        INNER JOIN worker_table w ON e.worker_id = w.id
                        INNER JOIN users u ON w.user_id = u.user_id
                        WHERE u.user_id = $userID
                        ORDER BY ending_date DESC";
                if($result = mysqli_query($conn, $sql)){
                    if(mysqli_num_rows($result) > 0){
                        $dataArray = array();
                        while ($row = mysqli_fetch_assoc($result)) {
                            array_push($dataArray, $row);
                        }
                        $response['response'] = "Success";
                        $response['data_array'] = $dataArray;
                        echo json_encode($response);
                    }else{
                        $response['response'] = "Error";
                        $response['message'] = "Experiences not found";
                        echo json_encode($response);
                    }
                }else{
                    $response['response'] = "Error";
                    $response['message'] = "Call error";
                    echo json_encode($response);
                }
            }else{
                $response['response'] = "Error";
                $response['message'] = "UserID not set";
                echo json_encode($response);
            }
            break;
        case "getWorkerProffession":
            if(isset($_POST['profession'])){
                $profession = $_POST['profession'];
                $sql = "SELECT u.user_id, w.address, u.name, u.surname FROM worker_table w
                INNER JOIN users u ON w.user_id = u.user_id
                WHERE w.profession_id=(SELECT profession_id FROM profession
                         WHERE profession_name = '$profession')  ORDER BY u.name ASC";
                if($result = mysqli_query($conn, $sql)){
                    if(mysqli_num_rows($result) > 0){
                        $dataArray = array();
                        while ($row = mysqli_fetch_assoc($result)) {
                            array_push($dataArray, $row);
                        }
                        $response['response'] = "Success";
                        $response['data_array'] = $dataArray;
                        echo json_encode($response);
                    }else{
                        $response['response'] = "Error";
                        $response['message'] = "Workers not found";
                        echo json_encode($response);
                    }
                }else{
                    $response['response'] = "Error";
                    $response['message'] = "Bad query";
                    echo json_encode($response);
                }
            }else{
                $response['response'] = "Error";
                $response['message'] = "Profession value not passed";
                echo json_encode($response);
            }
            break;
    }

}else{
    header("Location:/index.php");
}

?>