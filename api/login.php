<?php
    include 'connection.php';
    //returning json to php
    header('Content-Type: application/json');

if (isset($_POST['username']) && isset($_POST['password'])){

    $response = array();
    $username = $_POST['username'];
    $password = $_POST['password'];

    if(strlen($username) < 5) {
        $response['response'] = "Error";
        $response['message'] = "Username must be at least 5 digits";
        echo json_encode($response);
    }else if(strlen($password) < 8){
        $response['response'] = "Error";
        $response['message'] = "Password must be at least 8 digits";
        echo json_encode($response);
    }
    //regex to validate password
    else if (!preg_match('/[A-Za-z].*[0-9]|[0-9].*[A-Za-z]/', $password)) {
        $response['response'] = "Error";
        $response['message'] = "Password must contain letters and numbers";
        echo json_encode($response);
    }else{
        $accountCheck = mysqli_query($conn,"SELECT * FROM users WHERE username='$username'");
        if (mysqli_num_rows($accountCheck) != 0) {

            $account = mysqli_fetch_assoc($accountCheck);
            $serverPassword = $account['password'];

            $passMD5 = md5($password);
            if($passMD5 == $serverPassword){
                $loginData = array();
                $loginData['username'] =$account['username'];
                $loginData['dateRegistered'] = $account['dateRegistered'];
                $loginData['user_id'] = $account['user_id'];
                $loginData['role'] = $account['role'];

                $response["loginData"] = $loginData;
                $response["message"] = "User verified";
                $response["response"] = "Success";
                $response["user_id"] = $account['user_id'];
                echo json_encode($response);
                setcookie("LoggedIn", "".$account['user_id'], time() + (86400 * 30), "/");
                setcookie("LoggedInRole", "".$account['role'], time() + (86400 * 30), "/");

            }else{
                $response["message"] = "Incorrect Password";
                $response["response"] = "Error";
                echo json_encode($response);
            }
        }else{
            $response["message"] = "Incorrect Username";
            $response["response"] = "Error";
            echo json_encode($response);
        }
    }



}else{
    echo json_encode("Values not set");
}

?>