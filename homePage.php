<div class="container-fluid kk" style="background-color:white; margin:40px;">
    <div class="row" style="margin:50px">
        <div class="col-lg-10">
            <h2 class="pstyling" style=" font-family:Bellefair">Rreth nesh</h2>
            <h1 class="kstyling" style="font-family:Abril Fatface">Historia jone</h1>
            <p class="pstyling" style=" font-family:Bellefair"> Zanafilla e kesaj faqeje ka ardhur si rezultat i nje projekti shkollor
                ne lenden 'Pragramim web' caktuar per studentet e vitit te trete, dega Informatike.
                Projekti synonte krijimin e nje faqeje web qe do ti ofronte perdoruesit funksionalitetet
                qe nje website normal ofron.Grupi realizues i ketij projekti mendoi qe vec efikasitetit
                qe faqja do te ofronte vizulaisht te krijohej dhe nje faqe qe e sjelle ne jete do tu vinte
                perdoruesve ne ndihme edhe ne perditshmeri.Si rrjedhoje erdhi ideja e nje faqeje qe mundeson
                kerkimin e profesionisteve. Te gjithe ne nje menyre apo nje tjeter kemi nevoje per nje zanatci
                kur nje problem lind ne shtepi e jo te gjithe e kemi nje liste me numra telefonash te zanatcinjve
                ne celulare. Ky website ofron pikerisht mundesine e gjetes jo vetem te nje profesionisti ne kohe
                reale por dhe selektimin e atij qe gjenden afer nesh dhe permbush nevojat tona. Ajo cfare ofrojme
                me kete faqe eshte thjeshtesi ne perdorim, shpejtesi ne pergjigjen e kerkesave tuaja dhe besueshmeri
                mbi transparencen dhe vertetesine e asaj qe ofrojme.</p>
        </div>
        <div class="col-lg-2">
            <img src="images/thjeshtesi.png" class="img-responsive" style="width:60px;height:60px" >
            <p class="pstyling" style=" font-family:Bellefair">Thjeshtesi</p>
            <img src="images/shpejtesi.png" class="img-responsive" style="width:50px;height:50px" >
            <p class="pstyling"style=" font-family:Bellefair">Shpejtesi</p>
            <img src="images/besim.png" class="img-responsive" style="width:50px;height:50px" >
            <p class="pstyling"style=" font-family:Bellefair"> Besueshmeri</p>
        </div>
    </div>
</div>
<div class="container-fluid kk">
    <div class="row" style="margin:50px">
        <h2 class="pstyling" style=" font-family:Bellefair">Cfare ofrojme?</h2>
        <h1 class="kstyling font-family:Abril Fatface" >Sherbimet tona</h1>
    </div>
    <div class="row">
        <div class="col-lg-4"  id="hidraulik">
            <a href="search.php?prof='Hidraulik'" ><img src="images/hidraulik2.png" class="img-responsive" style="width:350px;height:230px" ></a>
        </div>
        <div class="col-lg-4"  id="elektricist">
            <a href="search.php?prof='Elektricist'" ><img src="images/elektricist2.png" class="img-responsive" style="width:350px;height:230px" ></a>
        </div>
        <div class="col-lg-4"  id="arkitekt">
            <a href="search.php?prof='Arkitekt'"><img src="images/arkitekt2.png" class="img-responsive" style="width:350px;height:230px" ></a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-4"  id="bojaxhi">
            <a href="search.php?prof='Bojaxhi'"><img src="images/bojaxhi2.png" class="img-responsive" style="width:350px;height:230px" ></a>
        </div>
        <div class="col-lg-4"  id="marangoz">
            <a href="search.php?prof='Marangoz'"><img src="images/marangoz2.png" class="img-responsive" style="width:350px;height:230px" ></a>
        </div>
        <div class="col-lg-4">
            <a href="search.php?prof='Teknik'"><img  src="images/teknik.png" class="img-responsive" style="width:350px;height:230px" ></a>
        </div>
    </div>
    <div class="row">
        <br><br>
    </div>

</div>

