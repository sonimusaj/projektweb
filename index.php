<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<title>Zanatci Ne Shtepi</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script type="text/javascript" src="js/functions.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Bellefair" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="mainpage.css">
  
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</head>

<body style="background-color:#F29200">
     <nav class="navbar"  >
          <div class="container-fluid">
               <ul class="nav navbar-nav" >
                  <li id="home_button" class="active"><a href="#">Home</a></li>
		          <li id="about_button" ><a href="#">About</a ></li>
				  <li id="profile"><a href="#">Profile</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                  <li><a href="#"><span class='glyphicon glyphicon-search'></span> <!-- <input type="text" id="search" placeholder=" Kerko profesion" />-->

                          <input id="search" placeholder="Kerko Punetore" list="names">
                          <datalist id="names">
                          </datalist>

                      </a></li>
                 <li id="sign_up_button"><a href="signup.php" ><span class="glyphicon glyphicon-user"></span>Sign up</a></li>
                 <li id="sign_in_button"><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
                 <li id="log_out"><a href="logout.php"><span class="glyphicon glyphicon-log-in"></span>Logout</a></li>
              </ul>
          </div>
		  <img id="main_page" src="images/back.png" >
     </nav>
    
        <div  class="page-container">
            <div id="main_container"></div>
        </div>

     <script type="text/javascript" src="js/main.js"></script>

     <script>
         $(document).ready(function () {
             $("#main_container").load("homePage.php");
         });
     </script>

     <footer class="container-fluid fstyle" >
        <br><br>
        <p class="pstyling" ><img src="https://image.flaticon.com/icons/png/512/34/34067.png"  width="20px" height="20px" > Email: zanatci@gmail.com</p>
        <p class="pstyling" ><img src="http://www.clker.com/cliparts/W/j/m/3/u/z/email-icon-hi.png" width="20px" height="20"> Cel:02488597357
        <p class="pstyling" style="text-align:center" > Find us on social medias: &nbsp <img src="https://image.flaticon.com/icons/png/512/69/69366.png" width="20px" height="20px">&nbsp <img src="https://image.flaticon.com/icons/svg/20/20673.svg" width="20px" height="20px"></p>
        <p class="pstyling" style="text-align:center" >&copy; 2017-2018 ZanatciNeShtepi.com<p>
    </footer>
</body>
</html>