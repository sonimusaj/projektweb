function login(username, password){

    if(username == "" || password == ""){
        alert("Please fill all fields")
    }else if(username.length < 5){
        alert("Username must contain at least 5 characters");
    }else if(password.length < 8){
        alert("Password must contain at least 8 characters");
    }else{
        var data = {username: username, password: password};
        $.post("api/login.php", data, function (data, status) {
            alert(data.message);
            if (data.response == "Success"){
                if (document.getElementById('remember_me').checked) {
                    localStorage.setItem("loggedInAccount", data.loginData.user_id);
                    localStorage.setItem("loggedInUsername", username);
                    localStorage.setItem("loggedInPassword", password);
                } else {
                    localStorage.setItem("loggedInAccount", "");
                    localStorage.setItem("loggedInUsername", "");
                    localStorage.setItem("loggedInPassword", "");
                }

                var role = data.loginData.role;
                if(role == "a"){
                    window.location.replace("dashboard.html");
                }else if(role == "w"){
                    checkIfRegistered(data.loginData.user_id);
                }else{
                    window.location.replace("index.php");
                }
            }
            else if(data.response == "Error"){
                alert(data.message);
            }
        });
    }
}


function checkIfRegistered($userID) {
    var data = {
        method: "checkWorkerLogIn",
        user_id: $userID
    };
    $.post('api/api.php', data, function (data, status) {
        if(data.response == "Success"){
            if(data.message == "User is not set"){
                window.location.replace("worker_details.php");
            }else if(data.message == "User is set"){
                window.location.replace("index.php");
            }
        } else if(data.response == "Error"){
            alert(data.message);
        }

    });

}

function saveWorkersData(){
    var job = parseInt($('#job').val())+1;
    var address = $('#address').val();
    var description = $('#worker_description').val() + "";
    var data = {
        method: "saveWorkersData",
        job: job,
        address: address,
        description: description
    };
    $.post('api/api.php', data, function (data) {
        if(data.response = "Success"){
            window.location.replace("index.php");
        }else if(data.response = "Error"){
            alert(data.message);
        }
    });
}

function requestProfessions(){
    getLoggedInUserId();
    $.post('api/api.php', {method: "requestProfessions"}, function (data) {
        if(data.response == "Success"){
            var professions = data.professions;
            $('#job').empty();
            for(var i = 0; i < professions.length; i++){
                $('#job').append($('<option>', {
                    value: i,
                    text:  professions[i]
                }));
            }
        }
    });
}

function getLoggedInUserId(){
    $.post('api/api.php', {method: "loggedInUser"}, function (data) {
        if(data.response == "Success"){
            alart(data.message + " ");
            return data.message;
        }else{
            return -1;
        }
    });
}

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

function checkCookieLoggedIn() {
    var loggedIn = getCookie("LoggedIn");
    var loggedInRole = getCookie("LoggedInRole");
    if (loggedIn != undefined && loggedIn != "" && (loggedInRole == "w" || loggedInRole == "c")) {
        return true;
    } else {
        return false;
    }
}

function getWorkerExperiences(userId) {
    var data = {
        method: "getWorkerExperiences",
        user_id: userId
    };
    $.post('api/api.php', data, function (data) {
        if(data.response == "Success"){
            var experiences = data.data_array;

            $("#profile_name").text(experiences[0]["name"] + ' ' + experiences[0]["surname"]);
            $("#profile_phone").html('<i class="fa fa-phone fa-fw w3-margin-right w3-large w3-text-teal"></i>' + experiences[0]["phone"]);
            $("#profile_address").html('<i class="fa fa-home fa-fw w3-margin-right w3-large w3-text-teal"></i>' + experiences[0]["address"]);

            for(var i = 0; i < experiences.length; i++){
                $('#exp_div').append('<div class="w3-container w3-card w3-white w3-margin-bottom">\n' +
                    '<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>' + experiences[i]["job_name"] + '</h2>\n' +
                    '\n' +
                    '<div class="w3-container">\n' +
                    '<h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i>' + experiences[i]["beginning_Date"] + ' - <span class="w3-tag w3-teal w3-round">' + experiences[i]["ending_Date"] + '</span></h6>\n' +
                    '<p>' + experiences[i]["description"] + '</p>\n' +
                    '<hr>\n' +
                    '</div>\n' +
                    '\n' +
                    '</div>');
            }

        }else{
            alert(data.message);
        }
    });
}

function getWorkerProffession(profession) {
    var data = {
        method: "getWorkerProffession",
        profession: profession
    };
    $.post('api/api.php', data, function (data) {
        if(data.response == "Success"){
            $('#profession').empty();
            var workers = data.data_array;
            for(var i = 0; i < workers.length; i++){
                var workerId = "" + workers[i]["user_id"];
                $('#profession').append('<div onclick="getWorkerById('+ workerId + ');" class="w3-container w3-card w3-white w3-margin-bottom">\n' +
                    '<h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i>' + workers[i]["name"] + ' ' + workers[i]["surname"] + '</h2>\n' +
                    '\n' +
                    '<div class="w3-container">\n' +
                    '</h6>\n' +
                    '<h4>Adresa: ' + workers[i]["address"] + '</h4>\n' +
                    '<hr>\n' +
                    '</div>\n' +
                    '\n' +
                    '</div>');
            }

        }
    });
}

function getWorkerById(workerId){
    window.location.href="profile.php?id='" + workerId + "'";

}

function addWorkerExperience(){
    var jobTitle = $("#job_name").val();
    var dateFrom = $("#date_from").val();
    var dateTo = $("#date_to").val();
    var jobDescription = $("#job_description").val();
    var user_id =  getCookie("LoggedIn");
    if(jobTitle == ""){
        alert("Please enter Job's Title");
        return;
    }

    var data = {
        method: "addWorkerExperience",
        jobTitle: jobTitle,
        dateFrom: dateFrom,
        dateTo: dateTo,
        jobDescription: jobDescription,
        user_id: user_id
    };
    $.post('api/api.php', data, function (data) {
        if(data.response = "Success"){
            window.location.replace("profile.php");
        }else if(data.response = "Error"){
            alert(data.message);
        }
    });
}


function registerUser(){
    var name = $('#name').val();
    var surname = $('#surname').val();
    var email = $('#email').val();
    var username = $('#username').val();
    var password = $('#password').val();
    var repeatedPassword = $('#password_repeat').val();
    var phone = $('#phone').val();
    var role = "c";
    var fromDashboard = $('#fromDashboard').val();
    email = email.toLowerCase();
    username = username.toLowerCase().trim();

    if(password == repeatedPassword){

        var data = {
            method: "registerUser",
            name: name,
            surname: surname,
            email: email,
            phone: phone,
            fromDashboard: fromDashboard,
            username: username,
            password: password,
            role: role
        };

        $.post('api/api.php', data, function (data, status) {
            if(data.response == "Error"){
                alert(data.message);
            }else{
                window.location.html = "index.php";
            }
        });

    }
    else{
        alert("Passwords don't match");
    }
}

function searchWorkers(name) {
    var data = {method: "searchWorkers",
        name: name};
    $.post("api/api.php", data, function (data, status) {
        if(data.response == "Success") {
            var workers = data.workers;
            $('#names').empty();
            $('#names').attr("onclick", "alert('workerID')");
            for(var i = 0; i < workers.length; i++){
                var workerID = workers[i]["user_id"];
                $('#names').append($('<option>', {
                    value: workers[i]['name'] + " " + workers[i]['surname'],
                    text:  workerID
                }));
            }
        }else{
            alert("Error: " + data.message);
        }
    });
}

