window.onload = function() {
    if(checkCookieLoggedIn){

        $("#profile").hide();
        $("#log_out").hide();
        if(getCookie("LoggedInRole") == "w"){
            //useri i loguar esht punetor
            $("#sign_up_button").hide();
            $("#sign_in_button").hide();
            $("#log_out").show();
            $("#profile").show();
        }else if(getCookie("LoggedInRole") == "c"){
            $("#sign_up_button").hide();
            $("#sign_in_button").hide();
            $("#log_out").show();
            $("#profile").hide();
        }
    }else{
        //nuk ka user te loguar
        $("#profile").hide();
        $("#log_out").hide();
        $("#sign_up_button").show();
        $("#sign_in_button").show();
    }

};
$(document).ready(function () {

    //$("#profile").hide();

    $("#signUp").click(function () {
        registerUser();
    });

    $("#login").click(function () {
        var username = $("#username").val();
        var password = $("#password").val();
        login(username, password);
    });

    $("#home_button").click(function () {
        $("#main_page").fadeIn();
        $("#main_container").load("homePage.php");
    });

    $("#about_button").click(function () {
        $("#main_page").fadeOut();
        $("#main_container").load("homePage.php");
    });

    $("#profile").click(function () {
        $("#main_page").fadeOut();
        window.location.href = "profile.php";

    });

    $("#contact_button").click(function () {
        $("#main_page").fadeOut();
        //$("#main_container").load("homePage.php");
    });

    $("#add_experience").click(function () {
        addWorkerExperience();
        $("#job_name").val("");
        $("#date_from").val("");
        $("#date_to").val("");
        $("#job_description").val("");
    });

    $("#finish").click(function () {
        saveWorkersData();
    });

    $("#log_out").click(function () {
        $("#profile").hide();
        $("#sign_up_button").show();
        $("#sign_in_button").show();
        $("#log_out").hide();
        window.location.href="logout.php";
    });

    $('#search').on('keypress', function (e) {
        if(e.which === 13){

            //Disable textbox to prevent multiple submit
            $(this).attr("disabled", "disabled");

            var enteredValue = $(this).val();
            searchWorkers(enteredValue);

            //Enable the textbox again if needed.
            $(this).removeAttr("disabled");
        }
    });


    //sidebar toggle
    $("#menu-toggle").click(function (toogle){
        toogle.preventDefault();
        $("#wrapper").toggleClass("sidebarOpened");
    });

    $('#search_tab').click(function () {
        $(this).attr("class", "active");
        $('#search_tab2').attr("class", "active");
        $('#add_tab').attr("class", "");
        $('#add_tab2').attr("class", "");
        $('#home_tab').attr("class", "");
        $('#home_tab2').attr("class", "");
    });

    $('#search_tab2').click(function () {
        $(this).attr("class", "active");
        $('#search_tab').attr("class", "active");
        $('#add_tab').attr("class", "");
        $('#add_tab2').attr("class", "");
        $('#home_tab').attr("class", "");
        $('#home_tab2').attr("class", "");
    });



    $('#home_tab').click(function () {
        $(this).attr("class", "active");
        $('#home_tab2').attr("class", "active");
        $('#add_tab').attr("class", "");
        $('#add_tab2').attr("class", "");
        $('#search_tab').attr("class", "");
        $('#search_tab2').attr("class", "");
    });

    $('#home_tab2').click(function () {
        $(this).attr("class", "active");
        $('#home_tab').attr("class", "active");
        $('#add_tab').attr("class", "");
        $('#add_tab2').attr("class", "");
        $('#search_tab').attr("class", "");
        $('#search_tab2').attr("class", "");
    });

    $('#add_tab').click(function () {
        $(this).attr("class", "active");
        $('#add_tab2').attr("class", "active");
        $('#search_tab2').attr("class", "");
        $('#search_tab').attr("class", "");
        $('#home_tab').attr("class", "");
        $('#home_tab2').attr("class", "");
        $('#container').load("worker_register.php");
    });

    $('#add_tab2').click(function () {
        $(this).attr("class", "active");
        $('#add_tab').attr("class", "active");
        $('#search_tab2').attr("class", "");
        $('#search_tab').attr("class", "");
        $('#home_tab').attr("class", "");
        $('#home_tab2').attr("class", "");
        $('#container').load("worker_register.php");
    });

});