<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en" >

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Zanatci Ne Shtepi</title>
    <script type="text/javascript" src="js/functions.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Bellefair" rel="stylesheet">
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="mainpage.css">
    <style>
    .vertical-center {
      min-height: 100%;  /* Fallback for browsers do NOT support vh unit */
      min-height: 100vh; /* These two lines are counted as one :-)       */
      display: flex;
      align-items: center;
    }
    .navbar-nav > li > a {
        color: black;
    }
    .navbar-nav > li > a:hover {
        background-color:#7396ff;
    }

    </style>
</head>

<body>

    <nav class="navbar"  >
         <div class="container-fluid">

             <ul class="nav navbar-nav" >
                 <li class="active"><a href="index.php">Home</a></li>
                 <li><a href="#">About</a></li>
                 <li><a href="#">Contact</a></li>
              </ul>
              <ul class="nav navbar-nav navbar-right">
                 <li><a href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign up</a></li>
              </ul>
          </div>
    </nav>
    <div class="jumbotron vertical-center" style="background-color:#c7d6ff">
          <div class="container" style="width:400px;background-color:white">
             <div class="wrapper">
                 <form class="form-signin">
                      <h2 class="form-signin-heading">Please login</h2>
                      <input type="text" autocomplete="on" class="form-control" id="username" name="username" placeholder="Email Address" required="" autofocus="" />
                      <input type="password" autocomplete="on" class="form-control" id="password" name="password" placeholder="Password" required=""/>
                     <input type="checkbox" value="remember-me" id="remember_me" name="rememberMe" > <span style="font-size:15px">Remember me</span>
                      <button id="login" class="btn btn-lg btn-primary btn-block" type="button">Login</button>
                     <input type="button" id="continue_as" class="btn btn-lg btn-primary btn-block" style="background-color: #F29200" value =""/><br/>
                  </form>
              </div>
         </div>
      </div>

    <script type="text/javascript" src="js/main.js"></script>

    <footer class="container-fluid fstyle" style="background-color:#4779ed" ><br>
    <p class="pstyling" ><img src="https://image.flaticon.com/icons/png/512/34/34067.png"  width="20px" height="20px" > Email: zanatci@gmail.com</p>
    <p class="pstyling" ><img src="http://www.clker.com/cliparts/W/j/m/3/u/z/email-icon-hi.png" width="20px" height="20"> Tel:02488597357
    <p class="pstyling" style="text-align:center" > Find us on social medias: &nbsp <img src="https://image.flaticon.com/icons/png/512/69/69366.png" width="20px" height="20px">&nbsp <img src="https://image.flaticon.com/icons/svg/20/20673.svg" width="20px" height="20px"></p>
    <p class="pstyling" style="text-align:center" >&copy; 2017-2018 ZanatciNeShtepi.com<p>
    </footer>

    <script>
        $(document).ready(function () {
            $("#continue_as").hide();

            if(localStorage.getItem("loggedInAccount") != ""){
                var data = {
                    method: "getUserByID",
                    user_id: "" + localStorage.getItem("loggedInAccount")
                };
                $.post('api/api.php', data, function (data, status) {
                    if(data.response == "Success"){
                        $("#continue_as").show();
                        $("#continue_as").val("Continue as " + data.user_name);
                    }else{
                        alert(data.message);
                    }
                });

            }

            $("#continue_as").click(function () {
                var username =  "" + localStorage.getItem("loggedInUsername");
                var password =  "" + localStorage.getItem("loggedInPassword");
                login(username, password);
            });
        });
    </script>


    <script>



    </script>




</body>

</html>