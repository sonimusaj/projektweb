<?php
if (isset($_COOKIE['LoggedIn']) || isset($_COOKIE['LoggedInRole'])) {
    setcookie("LoggedIn", "", time()-3600, "/");
    setcookie("LoggedInRole", "", time()-3600, "/");
    unset($_COOKIE['LoggedIn']);
    unset($_COOKIE['LoggedInRole']);

}
header("Location:index.php");
