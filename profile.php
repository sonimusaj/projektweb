<?php
    session_start();
?>
<!DOCTYPE html>
<html>
<title>Zanatci</title>
<meta charset="UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}
</style>
<body class="w3-light-grey">

<!-- Page Container -->
<div class="w3-content w3-margin-top" style="max-width:1400px;">

    <!-- The Grid -->
    <div class="w3-row-padding">

        <!-- Left Column -->
        <div class="w3-third">

            <button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.history.go(-1); return false;" style="float: right;background-color: #F29200; width: 100%">Back</button><br/><br/>

            <div class="w3-white w3-text-grey w3-card-4">
                <div class="w3-display-container">
                    <img src="images/login_background.png" style="width:100%" alt="Avatar">
                    <div class="w3-display-bottomleft w3-container w3-text-black">
                        <h2 id="profile_name"></h2>
                    </div>
                </div>
                <div class="w3-container">
                    <p></p>
                    <p id="profile_address"></p>
                    <p id="profile_phone"></p>
                    <hr>

                    <br>
                </div>
            </div><br>

            <!-- End Left Column -->
        </div>

        <!-- Right Column -->
        <div class="w3-twothird" id="exp_div">

            <div id="add_work_container" class="w3-container w3-card w3-white w3-margin-bottom">
                <br/><button class="btn btn-lg btn-primary btn-block" type="button" id="add_experience" style="float: right;background-color: #4CAF50; width: 200px;">Add Experience</button><br>
                <h2 class="w3-text-grey w3-padding-16"><i class="fa fa-suitcase fa-fw w3-margin-right w3-xxlarge w3-text-teal"></i><input type="text" id="job_name" placeholder="Job Title"/></h2>

                <div class="w3-container">
                    <h6 class="w3-text-teal"><i class="fa fa-calendar fa-fw w3-margin-right"></i><input type="date" id="date_from" placeholder="From"/> - <input type="date" id="date_to" placeholder="From"/></span></h6>
                    <textarea id="job_description" cols="100" rows="3" placeholder="Job Description"></textarea>
                    <hr>
                </div>

            </div>

            <!-- End Right Column -->
        </div>

        <!-- End Grid -->
    </div>

    <!-- End Page Container -->
</div>

<script>
    $(document).ready(function () {
       var workerId = "<?php if(isset($_GET['id']))
                            echo $_GET['id'];
                            else echo ''?>";

        if(workerId != ""){
            workerId = workerId.replace("'", "");
            var id = parseInt(workerId);
            $('#add_experience').hide();
            $('#add_work_container').hide();
            getWorkerExperiences(id);
        }else {
            if(checkCookieLoggedIn){
                if(getCookie("LoggedInRole") == "w"){
                    //useri i loguar esht punetor
                    $('#add_experience').show();
                    $('#add_work_container').show();
                    getWorkerExperiences(getCookie("LoggedIn"));
                }else if(getCookie("LoggedInRole") == "c"){
                    $('#add_experience').hide();
                    $('#add_work_container').hide();
                }
            }else{
                //nuk ka user te loguar
                $('#add_experience').hide();
                $('#add_work_container').hide();
            }
        }



        });
</script>

<script type="text/javascript" src="js/main.js"></script>

</body>
</html>
