<?php
session_start();
$profession = "";
if(isset($_GET['prof'])){
    $profession = $_GET['prof'];
}
?>
<!DOCTYPE html>
<html>
<title>Zanatci</title>
<meta charset="UTF-8">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/functions.js"></script>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
    html,body,h1,h2,h3,h4,h5,h6 {font-family: "Roboto", sans-serif}
</style>
<body class="w3-light-grey">

<!-- Page Container -->
<div class="w3-content w3-margin-top" style="max-width:1400px;">

    <!-- The Grid -->
    <div class="w3-row-padding">

        <!-- Left Column -->
        <div class="w3-third">

            <button class="btn btn-lg btn-primary btn-block" type="button" onclick="window.history.go(-1); return false;" style="float: right;background-color: #F29200; width: 100%">Back</button><br/><br/>

            <div class="w3-white w3-text-grey w3-card-4">
                <div class="w3-display-container">
                    <img id="image" style="width:100%" alt="Avatar">
                    <div class="w3-display-bottomleft w3-container w3-text-black">
                    </div>
                </div>
            </div><br>

            <!-- End Left Column -->
        </div>

        <!-- Right Column -->
        <div class="w3-twothird" id="profession">

            <div id="" class="w3-container w3-card w3-white w3-margin-bottom">
            </div>

            <!-- End Right Column -->
        </div>

        <!-- End Grid -->
    </div>

    <!-- End Page Container -->
</div>

<script>
    $(document).ready(function () {
        var profession = "<?php echo $profession;?>";
        profession = profession.replace("'", "");
        profession = profession.replace("'", "");
        $("#image").attr("src", "images/hidraulik2.png");
        if (profession == "Hidraulik") {
            $("#image").attr("src", "images/hidraulik2.png");
        }
        else if(profession =="Elektricist"){
            $("#image").attr("src", "images/elektricist2.png");
        }
        else if(profession == "Arkitekt"){
            $("#image").attr("src", "images/arkitekt2.png");
        }
        else if(profession == "Bojaxhi"){
            $("#image").attr("src", "images/bojaxhi2.png");
        }
        else if(profession == "Marangoz"){
            $("#image").attr("src", "images/marangoz2.png");
        }
        else if(profession == "Teknik"){
            $("#image").attr("src", "images/teknik.png");
        }
        else{
            $("#image").attr("src", "images/teknik.png");
        }

        getWorkerProffession(profession);
    });
</script>

</body>
</html>
