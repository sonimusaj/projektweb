<?php
    session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Zanatci Ne Shtepi</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="text/javascript" src="js/functions.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/md5.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" type="text/css" href="mainpage.css">
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Bellefair" rel="stylesheet">
    <style>
        .navbar-nav > li > a {
            color: black;
        }
        .navbar-nav > li > a:hover {
            background-color:#6aff96;
        }
    </style>
</head>

<body style="background-color:#6aff96">

<nav class="navbar">
    <div class="container-fluid">
        <ul class="nav navbar-nav" >
            <li class="active"><a href="index.php">Home</a></li>
            <li><a href="#">About</a></li>
            <li><a href="#">Contact</a></li>
        </ul>
        <ul class="nav navbar-nav navbar-right">
            <li><a href="signup.php"><span class="glyphicon glyphicon-user"></span>Sign up</a></li>
            <li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span>Login</a></li>
        </ul>
    </div>
</nav>

<div class="jumbotron vertical-center" style="background-color:#6aff96">
    <div class="container" style="width:400px;background-color:white">
        <div class="wrapper">
            <p>Please enter your information</p>
            <form class="form-signin" style="font-size:15px" ><br>
                <label>Job</label>
                <br/>
                <select id="job">
                </select><br/>
                <label>Address</label><input type="text" class="form-control" placeholder="Enter your address" id="address" required>
                <label>Personal Description</label><br/><Textarea id="worker_description" cols="43" rows="3" placeholder="Write something about you (Optional)"></Textarea>
                <br/>
                <br/>
                <button class="btn btn-lg btn-primary btn-block" type="button" id="finish">Finish</button><br>
            </form>
        </div>
    </div>
</div>

<script>

    $(document).ready(function () {
        requestProfessions();
    });
</script>



<script type="text/javascript" src="js/main.js"></script>

<footer class="container-fluid fstyle" style="background-color:#4779ed" ><br>
    <p class="pstyling" ><img src="https://image.flaticon.com/icons/png/512/34/34067.png"  width="20px" height="20px" > Email: randomemail@gmail.com</p>
    <p class="pstyling" ><img src="http://www.clker.com/cliparts/W/j/m/3/u/z/email-icon-hi.png" width="20px" height="20"> Tel:02488597357
    <p class="pstyling" style="text-align:center" > Find us on social medias: &nbsp <img src="https://image.flaticon.com/icons/png/512/69/69366.png" width="20px" height="20px">&nbsp <img src="https://image.flaticon.com/icons/svg/20/20673.svg" width="20px" height="20px"></p>
    <p class="pstyling" style="text-align:center" >&copy; 2017-2018 ZanatciNeShtepi.com<p>
</footer>
</body>
</html>