<?php
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Zanatci Ne Shtepi</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <script type="text/javascript" src="js/functions.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/md5.js"></script>
        <link rel='stylesheet prefetch' href='http://netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css'>
    <link href="https://fonts.googleapis.com/css?family=Abril+Fatface|Bellefair" rel="stylesheet">
    <style>
        .navbar-nav > li > a {
            color: black;
        }
        .navbar-nav > li > a:hover {
            background-color: #6aff96;
        }
    </style>
</head>


<body style="background-color:#6aff96">


<div class="jumbotron vertical-center" style="background-color:#6aff96">
    <div class="container" style="width:400px;background-color:white">
        <div class="wrapper">
            <form class="form-signin" style="font-size:15px; background-color: #fff; padding: 20px;" ><br>
                <!--<button name="b1" class="btn btn-lg btn-primary btn-inline-block"  onclick="f1()" >Perdorues</button>
                <button name="b2" class="btn btn-lg btn-primary btn-inline-block" onclick="f2()" >Profesionist</button><br>-->
                <label>Name</label><input type="text" class="form-control" id="name" placeholder="Name" required="" autofocus="" />
                <input type="hidden" class="form-control" id="fromDashboard" value="true" />
                <label>Surname</label><input type="text" class="form-control" id="surname" placeholder="Surname" required=""/>
                <label>Email</label><input type="text" class="form-control" placeholder="Enter Email" id="email" required>
                <label>Username</label><input type="text" class="form-control" placeholder="Enter Username" id="username" required>
                <label>Password</label><input type="password" class="form-control" placeholder="Enter Password" id="password"
                                              required>
                <label>Repeat Password</label><input type="password" class="form-control" placeholder="Repeat Password" id="password_repeat" required>
                <!--<div id="city"><label>City</label><input type="text" class="form-control" placeholder="City" name="city"  required></div>-->
                <div id="tel"><label>Tel</label><input type="text" class="form-control" placeholder="Telephone" id="phone" ><br /></div>

                <button class="btn btn-lg btn-primary btn-block" type="button" id="signUp">Sign Up</button><br>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="js/main.js"></script>
</body>